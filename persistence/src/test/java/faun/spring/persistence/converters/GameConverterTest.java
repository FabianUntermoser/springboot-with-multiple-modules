package faun.spring.persistence.converters;

import faun.spring.domain.Developer;
import faun.spring.domain.Game;
import faun.spring.domain.Publisher;
import faun.spring.persistence.dto.GameDTO;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class GameConverterTest {

    @Test
    public void testNullObject() {
        GameConverter converter = new GameConverter();
        assertNull(converter.convert(null));
    }

    @Test
    public void testConvertGame() {
        GameConverter converter = new GameConverter();

        Game game = new Game();
        game.setId(1L);
        game.setTitle("Game Title");
        game.setGenre("Genre");

        List<Developer> developers = new ArrayList<>();
        Developer developer = new Developer();
        developer.setId(2L);
        developer.setName("Dev");
        developer.setGames(Collections.singletonList(game));
        developers.add(developer);
        game.setDevelopers(developers);

        List<Publisher> publishers = new ArrayList<>();
        Publisher publisher = new Publisher();
        publisher.setId(3L);
        publisher.setName("Pub");
        publisher.setGames(Collections.singletonList(game));
        publishers.add(publisher);
        game.setPublishers(publishers);

        GameDTO gameDTO = converter.convert(game);

        assertEquals(game.getId(), gameDTO.getId());
        assertEquals(game.getTitle(), gameDTO.getTitle());
        assertEquals(game.getGenre(), gameDTO.getGenre());
        assertEquals(game.getDevelopers().size(), gameDTO.getDevelopers().size());
        assertEquals(game.getPublishers().size(), gameDTO.getPublishers().size());
    }

}