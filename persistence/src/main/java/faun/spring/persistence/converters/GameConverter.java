package faun.spring.persistence.converters;

import faun.spring.domain.Game;
import faun.spring.persistence.dto.GameDTO;

public class GameConverter {
    public GameDTO convert(Game game) {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setId(game.getId());
        gameDTO.setTitle(game.getTitle());
        gameDTO.setGenre(game.getGenre());
//        gameDTO.setDevelopers(game.getDevelopers());
//        gameDTO.setPublishers(game.getPublishers());
        return null;
    }
}
