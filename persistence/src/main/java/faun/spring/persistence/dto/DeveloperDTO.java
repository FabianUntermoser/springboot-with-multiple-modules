package faun.spring.persistence.dto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class DeveloperDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "developers")
    private List<GameDTO> games;

    public DeveloperDTO() {
        this.games = new ArrayList<>();
    }

    public DeveloperDTO(String name) {
        this();
        this.name = name;
    }

    public DeveloperDTO(String name, List<GameDTO> games) {
        this.name = name;
        this.games = games;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GameDTO> getGames() {
        return games;
    }

    public void setGames(List<GameDTO> games) {
        this.games = games;
    }
}
