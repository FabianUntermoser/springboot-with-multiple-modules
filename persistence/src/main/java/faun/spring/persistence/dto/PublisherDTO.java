package faun.spring.persistence.dto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class PublisherDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "publishers")
    private List<GameDTO> games;

    public PublisherDTO() {
        this.games = new ArrayList<>();
    }

    public PublisherDTO(String name) {
        this();
        this.name = name;
    }

    public PublisherDTO(String name, List<GameDTO> games) {
        this.name = name;
        this.games = games;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GameDTO> getGames() {
        return games;
    }

    public void setGames(List<GameDTO> games) {
        this.games = games;
    }

}
