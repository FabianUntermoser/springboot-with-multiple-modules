package faun.spring.services;

import faun.spring.domain.Game;

import java.util.List;

public interface GameService {

    List<Game> getGames();

}
