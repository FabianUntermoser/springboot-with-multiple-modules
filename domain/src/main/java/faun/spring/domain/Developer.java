package faun.spring.domain;

import java.util.ArrayList;
import java.util.List;

public class Developer {

    private Long id;
    private String name;
    private List<Game> games;

    public Developer() {
        this.games = new ArrayList<>();
    }

    public Developer(String name) {
        this();
        this.name = name;
    }

    public Developer(String name, List<Game> games) {
        this.name = name;
        this.games = games;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }
}
