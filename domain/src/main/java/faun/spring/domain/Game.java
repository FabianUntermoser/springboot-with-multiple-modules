package faun.spring.domain;

import java.util.List;

public class Game {

    private Long id;
    private String title;
    private String genre;
    private int price;
    private List<Publisher> publishers;
    private List<Developer> developers;

    public Game() {
    }

    public Game(String title, String genre, int price, List<Publisher> publishers, List<Developer> developers) {
        this.title = title;
        this.genre = genre;
        this.price = price;
        this.publishers = publishers;
        this.developers = developers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<Publisher> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<Publisher> publishers) {
        this.publishers = publishers;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }
}
