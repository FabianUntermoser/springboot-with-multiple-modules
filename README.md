#springboot-with-multiple-modules

This is a spring boot project with multiple maven modules for fun.

Modules:

 * **Domain Module**: Keeps all of the domain objects.
 * **Persistence Module**: Contains all of the persistence objects and DTO converters. 
 * **Web Module**: Contains Spring Logic and User Interface

##Application Logic

The goal is to provide a steam like user interface for games, developers, publishers and the like.

### Requirements

 1. The user should be able to login
 2. The user can search for games in the store
 3. The user can buy games, which are then placed in his own inventory
 4. The user can download games, which he has bought
 5. The user can switch his language and see translations
 6. The publisher can create, edit, delete or publish his own games

### Domain Objects

 - Game
    - title
    - genre
    - price
    - List of languages
    - List of developers
    - List of publishers
 - Developer
    - name
    - List of games
 - Publisher
    - name
    - List of games
 - Language Enum
 - Genre Enum
 - User
    - userName
    - password (securely stored)
    - money
    - List of games