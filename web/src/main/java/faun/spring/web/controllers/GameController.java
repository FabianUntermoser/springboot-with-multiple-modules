package faun.spring.web.controllers;

import faun.spring.services.GameService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GameController {

    private GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping("/games")
    public String getGames(Model model) {
        System.out.println("Opening Games Facelet");
        model.addAttribute("games", gameService.getGames());
        return "games";
    }

}
