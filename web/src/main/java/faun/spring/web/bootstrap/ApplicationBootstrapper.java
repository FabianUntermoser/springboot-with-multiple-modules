package faun.spring.web.bootstrap;

import faun.spring.domain.Developer;
import faun.spring.domain.Game;
import faun.spring.domain.Publisher;
import faun.spring.persistence.repositories.DeveloperRepository;
import faun.spring.persistence.repositories.GameRepository;
import faun.spring.persistence.repositories.PublisherRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ApplicationBootstrapper implements ApplicationListener<ContextRefreshedEvent> {

    private GameRepository gameRepository;
    private PublisherRepository publisherRepository;
    private DeveloperRepository developerRepository;

    public ApplicationBootstrapper(GameRepository gameRepository, PublisherRepository publisherRepository, DeveloperRepository developerRepository) {
        this.gameRepository = gameRepository;
        this.publisherRepository = publisherRepository;
        this.developerRepository = developerRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        System.out.println("Bootstraping event");

        List<Publisher> publishers = new ArrayList<>();
        Publisher guava = new Publisher("Guava");
        Publisher huaweii = new Publisher("Huaweii");
        publishers.add(guava);
        publishers.add(huaweii);
        publisherRepository.saveAll(publishers);

        List<Developer> developers = new ArrayList<>();
        Developer marx = new Developer("Hastalavista Marx");
        Developer live = new Developer("Ableton Live");
        developers.add(marx);
        developers.add(live);
        developerRepository.saveAll(developers);

        Game ducks = new Game("Shoot the duck", "4Kids", 666, publishers, developers);
        Game tetris = new Game("Tetris", "4Grannies", 99, publishers, developers);

        List<Game> games = new ArrayList<>();
        games.add(ducks);
        games.add(tetris);

        gameRepository.saveAll(games);
//        games.forEach(game -> {
//            game.getPublishers().forEach(publisher -> publisher.getGames().add(game));
//            game.getDevelopers().forEach(developer -> developer.getGames().add(game));
//        });

    }

    private Game bootstrapGame(Game game) {
        game.getPublishers().forEach(publisher -> publisher.getGames().add(game));
        game.getDevelopers().forEach(developer -> developer.getGames().add(game));
        return game;
    }

}
