package faun.spring.web.rest;

import faun.spring.domain.Game;
import faun.spring.services.GameService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GameRestController {

    private GameService gameService;

    public GameRestController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/rest/games")
    private List<Game> getGames() {
        return gameService.getGames();
    }

}
